package LN;

import java.util.Date;

public class clsIncidencias {
	private int cliente_DNI;
	private int cliente_Usuarioo_idUsuario;
	private String administrador_permisos;
	private int administrador_Usuario_idUsuario;
	private Date fecha;
	private String descripcionIncidencias;
	private String ticketStatus;
	
	public clsIncidencias(int cliente_DNI, int cliente_Usuarioo_idUsuario, String administrador_permisos,
			int administrador_Usuario_idUsuario, Date fecha, String descripcionIncidencias, String ticketStatus) {
		super();
		this.cliente_DNI = cliente_DNI;
		this.cliente_Usuarioo_idUsuario = cliente_Usuarioo_idUsuario;
		this.administrador_permisos = administrador_permisos;
		this.administrador_Usuario_idUsuario = administrador_Usuario_idUsuario;
		this.fecha = fecha;
		this.descripcionIncidencias = descripcionIncidencias;
		this.ticketStatus = ticketStatus;
	}

	public int getCliente_DNI() {
		return cliente_DNI;
	}

	public void setCliente_DNI(int cliente_DNI) {
		this.cliente_DNI = cliente_DNI;
	}

	public int getCliente_Usuarioo_idUsuario() {
		return cliente_Usuarioo_idUsuario;
	}

	public void setCliente_Usuarioo_idUsuario(int cliente_Usuarioo_idUsuario) {
		this.cliente_Usuarioo_idUsuario = cliente_Usuarioo_idUsuario;
	}

	public String getAdministrador_permisos() {
		return administrador_permisos;
	}

	public void setAdministrador_permisos(String administrador_permisos) {
		this.administrador_permisos = administrador_permisos;
	}

	public int getAdministrador_Usuario_idUsuario() {
		return administrador_Usuario_idUsuario;
	}

	public void setAdministrador_Usuario_idUsuario(int administrador_Usuario_idUsuario) {
		this.administrador_Usuario_idUsuario = administrador_Usuario_idUsuario;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDescripcionIncidencias() {
		return descripcionIncidencias;
	}

	public void setDescripcionIncidencias(String descripcionIncidencias) {
		this.descripcionIncidencias = descripcionIncidencias;
	}

	public String getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}
		
}
