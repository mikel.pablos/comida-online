package LN;

import java.sql.Date;

public class clsPedido {
	private int idPedido;
	private String cliente_DNI;
	private Date fechaPedido;
	private double precio;
	private String restaurante_nombre;
	
	public clsPedido(int idPedido, Date fechaPedido, double precio, String cliente_DNI, String restaurante_nombre) {
		super();
		this.idPedido = idPedido;
		this.fechaPedido = fechaPedido;
		this.precio = precio;
		this.cliente_DNI = cliente_DNI;
		this.restaurante_nombre = restaurante_nombre;
	}

	public int getIdPedido() {
		return idPedido;
	}

	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}

	public Date getFechaPedido() {
		return fechaPedido;
	}

	public void setFechaPedido(Date fechaPedido) {
		this.fechaPedido = fechaPedido;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getCliente_DNI() {
		return cliente_DNI;
	}

	public void setCliente_DNI(String cliente_DNI) {
		this.cliente_DNI = cliente_DNI;
	}

	public String getRestaurante_nombre() {
		return restaurante_nombre;
	}

	public void setRestaurante_nombre(String restaurante_nombre) {
		this.restaurante_nombre = restaurante_nombre;
	}
	
}
