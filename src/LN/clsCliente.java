package LN;

import java.util.HashMap;

public class clsCliente extends clsUsuario{
	private String DNI;
	private String nombre;
	private String apellido;
	private int tarjetaCredito;
	private String direccion;
	private int telefono;
	private String correoElectronico;
	
	public clsCliente(int idUsuario, String nombreUsuario, String contraseña, String DNI, String nombre, String apellido,int tarjetaCredito, String direccion, int telefono, String correoElectronico) {
		super(idUsuario, nombreUsuario, contraseña);
		// TODO Auto-generated constructor stub
		this.setDNI(DNI);
		this.nombre = nombre;
		this.apellido = apellido;
		this.tarjetaCredito = tarjetaCredito;
		this.direccion = direccion;
		this.telefono = telefono;
		this.correoElectronico = correoElectronico;
	}

	public String getDNI() {
		return DNI;
	}

	public void setDNI(String dNI) {
		try{
			if(dNI.length() == 8) {
				this.DNI = dNI;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getTarjetaCredito() {
		return tarjetaCredito;
	}

	public void setTarjetaCredito(int tarjetaCredito) {
		this.tarjetaCredito = tarjetaCredito;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	
	public boolean dniLength() {
		if(this.DNI.length() == 9) {
			return true;
		}
		return false;
	}
	
}
