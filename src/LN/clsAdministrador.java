package LN;

public class clsAdministrador extends clsUsuario{
	
	private String permisos;
	private String adminKey;
	
	public clsAdministrador(int idUsuario, String nombreUsuario, String contraseña, String permisos, String adminKey) {
		super(idUsuario, nombreUsuario, contraseña);
		this.permisos = permisos;
		this.adminKey = adminKey;
	}

	public String getPermisos() {
		return permisos;
	}

	public void setPermisos(String permisos) {
		this.permisos = permisos;
	}

	public String getAdminKey() {
		return adminKey;
	}

	public void setAdminKey(String adminKey) {
		this.adminKey = adminKey;
	}
	
}
