package LN;

import java.util.ArrayList;

public class clsGestorLN {
	 static ArrayList<clsCliente> listaClientes = new ArrayList<clsCliente>();
	
	 public static boolean registro(String user, String password, String nombre, String apellidos, String dni, int tarjetaCredito, String direccion, int telefono, String correoElectronico) {
		 try {
			Boolean existe = false;
			for (clsCliente clsCliente : listaClientes) {
				if(dni.equals(clsCliente.getDNI())) {
					existe = true;
					System.out.println("Ya existe un usuario con el mismo DNI");
					return false;
				}
			}
			if (existe != true) {
				int id = listaClientes.size() + 1;
				listaClientes.add(new clsCliente(id, user, password, dni, nombre, apellidos, tarjetaCredito, direccion, telefono, correoElectronico));
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	 }

	public static boolean comprobarLogIn(String nombreUsuario, String contraseña) {
		for (clsCliente cliente : listaClientes) {
			if(cliente.getNombreUsuario().equals(nombreUsuario)) {
				if(cliente.getContraseña().equals(contraseña)) {
					return true;
				}	
			}
		}
		return false;
	}
}
