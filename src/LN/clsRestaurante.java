package LN;

public class clsRestaurante {
	private String nombre;
	private String fechaFundacion;
	private String calle;
	private String tipoComida;
	
	public clsRestaurante(String nombre, String fechaFundacion, String calle, String tipoComida) {
		super();
		this.nombre = nombre;
		this.fechaFundacion = fechaFundacion;
		this.calle = calle;
		this.tipoComida = tipoComida;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFechaFundacion() {
		return fechaFundacion;
	}

	public void setFechaFundacion(String fechaFundacion) {
		this.fechaFundacion = fechaFundacion;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getTipoComida() {
		return tipoComida;
	}

	public void setTipoComida(String tipoComida) {
		this.tipoComida = tipoComida;
	}
	
	
}
