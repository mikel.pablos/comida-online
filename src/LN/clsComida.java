package LN;

public class clsComida {
	private String nombre;
	private double precio;
	private String descripcion;
	private String restaurante_nombre;
	
	public clsComida(String nombre, double precio, String descripcion, String restaurante_nombre) {
		super();
		this.nombre = nombre;
		this.precio = precio;
		this.descripcion = descripcion;
		this.restaurante_nombre = restaurante_nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getRestaurante_nombre() {
		return restaurante_nombre;
	}

	public void setRestaurante_nombre(String restaurante_nombre) {
		this.restaurante_nombre = restaurante_nombre;
	}

}
