package LP;

import java.util.Scanner;

import LN.clsGestorLN;

public class clsGestorLP {

	public void menu() {
		String respuesta;
		Boolean fin = true;
		do {
			System.out.println("¿Qué desea hacer?");
			System.out.println("1. log in");
			System.out.println("2. register");
			System.out.println("3. exit");
			Scanner entrada = new Scanner(System.in);
			respuesta = entrada.nextLine();
			switch (respuesta){
			case "1":
				if(this.logIn()) {
					this.menuUsuario();
					fin = false;
				}else{
					System.out.println("usuario o contraseña incorrecta");
					fin = true;
				};
				break;
			case "2":
				if(this.register()) {
					System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
					System.out.println("te has registrado correctamente");
					System.out.println("\n\n\n");
					System.out.println("Iniciar sesion");
					if(this.logIn()) {
						System.out.println("Sesion iniciada con exito");
					}
					fin = false;
				}else{
					System.out.println("ha habido un error, vuelve a intentarlo más tarde");
					fin = true;
				}
				break;
			case "3":
				System.out.println("programa finalizado");
				fin = false;
				break;
			default:
				System.out.println("no es un valor válido");
				break;
			}
		}while(fin);
	}

	private boolean logIn() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("introduce tu nombre de usuario");
		String nombreUsuario = scanner.nextLine();
		System.out.println("introduce tu contraseña");
		String contraseña = scanner.nextLine();
		return clsGestorLN.comprobarLogIn(nombreUsuario,contraseña);
	}
	
	private boolean register() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Introduzca el nombre de usuario");
		String user = scanner.nextLine();
		System.out.println("Introduzca una contraseña");
		String password = scanner.nextLine();
		System.out.println("Introduzca el nombre");
		String nombre = scanner.nextLine();
		System.out.println("Introduzca el apellido");
		String apellidos = scanner.nextLine();
		System.out.println("Introduzca la tarjetaCredito");
		int tarjetaCredito = Integer.parseInt(scanner.nextLine());
		System.out.println("Introduzca la dirección");
		String direccion = scanner.nextLine();
		System.out.println("Introduzca el número de teléfono");
		int telefono = Integer.parseInt(scanner.nextLine());
		System.out.println("Introduzca el correoElectrónico");
		String correoElectronico = scanner.nextLine();
		System.out.println("Introduzca el DNI");
		String dni = scanner.nextLine();
		return clsGestorLN.registro(user,password,dni,nombre,apellidos,tarjetaCredito,direccion,telefono,correoElectronico);
	}
	
	private void menuUsuario() {
		String respuesta;
		boolean fin = true;
		do {
		Scanner scanner = new Scanner(System.in);
		System.out.println("¿Qué desea hacer?");
		System.out.println("1. realizar un pedido");
		System.out.println("2. abrir un ticket de reclamación");
		System.out.println("3. salir");
		respuesta = scanner.nextLine();
		switch(respuesta) {
		case "1":
			break;
		case "2":
			break;
		case "3":
			break;
		default:
			System.out.println("no es una opción válida");
			break;
		}
		}while(fin);
	}
}
